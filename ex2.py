#nir yarkoni 313587057
from Perceptron import Perceptron
from PassiveAggressive import PassiveAggressive
from SVM import SVM
import sys

import numpy as np
import numpy.core.defchararray as npCr


# def min_max_norm_training(exTraining):
#     for i in range(0, exTraining.shape[1]):
#         if ((exTraining[:, i].max() - exTraining[:, i].min()) != 0):
#             exTraining[:, i] = ((exTraining[:, i] - exTraining[:, i].min()) /
#                                 (exTraining[:, i].max() - exTraining[:, i].min()))
#
#
# def min_max_norm_test(exTraining, exTest):
#     for i in range(0, exTraining.shape[1]):
#         if ((exTraining[:, i].max() - exTraining[:, i].min()) != 0):
#             exTest[:, i] = ((exTest[:, i] - exTraining[:, i].min()) /
#                             (exTraining[:, i].max() - exTraining[:, i].min()))

#test the test-set after the learning
def test(wVectors, test_set):
    results = []
    size = len(test_set)
    for i in range(0, size):

        argMax = 0
        predicted_class = 0
        #find the arg max
        for classI in range(3):
            y_Tag = np.dot(test_set[i], wVectors[classI])
            if (y_Tag >= argMax):
                argMax = y_Tag
                predicted_class = classI
        results.append(predicted_class)
    return results

# print all the predicted classes
def print_predicted_classes(size_test_set, perceptron_predict, svm_predict, pa_predict):
    for j in range(size_test_set):
        perceptron = str(perceptron_predict[j])
        svm = str(svm_predict[j])
        pa = str(pa_predict[j])
        print("perceptron: " + perceptron + ", svm: " + svm + ", pa: " + pa)


if __name__ == '__main__':
    #read the "train_x"
    x_train = np.genfromtxt(sys.argv[1], dtype='str', delimiter=",")
    x_train = npCr.replace(x_train, 'M', '0.2')
    x_train = npCr.replace(x_train, 'F', '0.4')
    x_train = npCr.replace(x_train, 'I', '0.6')
    x_train = x_train.astype(np.float)
    # min_max_norm_training(x_train)

    #read the "train_y"
    y_train = np.genfromtxt(sys.argv[2], dtype='str', delimiter=",")
    y_train = [float(x) for x in y_train]
    y_train = [int(x) for x in y_train]

    #read the "test_x"  test set
    test_set = np.genfromtxt(sys.argv[3], dtype='str', delimiter=",")
    test_set = npCr.replace(test_set, 'M', '0.2')
    test_set = npCr.replace(test_set, 'F', '0.4')
    test_set = npCr.replace(test_set, 'I', '0.6')
    test_set = test_set.astype(np.float)

    # x_train_for_test = np.genfromtxt(sys.argv[1], dtype='str', delimiter=",")
    # x_train_for_test = npCr.replace(x_train_for_test, 'M', '0.2')
    # x_train_for_test = npCr.replace(x_train_for_test, 'F', '0.4')
    # x_train_for_test = npCr.replace(x_train_for_test, 'I', '0.6')
    # x_train_for_test = x_train_for_test.astype(np.float)

    # min_max_norm_test(x_train_for_test, test_set)

    test_label = []
    perceptron_predict = []
    svm_predict = []
    pa_predict = []
    size_test_set = len(test_set)

    # Perceptron test
    abalone_classifier_Perceptron = Perceptron(x_train, y_train)
    afterTraining_Perceptron = abalone_classifier_Perceptron.train()
    perceptron_predict = test(afterTraining_Perceptron, test_set)

    # SVM test
    abalone_classifier_SVM = SVM(x_train, y_train)
    afterTraining_SVM = abalone_classifier_SVM.train()
    svm_predict = test(afterTraining_SVM, test_set)

    # PA test
    abalone_classifier_PA = PassiveAggressive(x_train, y_train)
    afterTraining_PA = abalone_classifier_PA.train()
    pa_predict = test(afterTraining_PA, test_set)

    print_predicted_classes(size_test_set, perceptron_predict, svm_predict, pa_predict)
