#nir yarkoni 313587057
import numpy as np
import random

ITERATIONS = 20


class SVM(object):
    def __init__(self, exTraining, labelTraining, iterationsNum=ITERATIONS):
        self.exTraining = exTraining
        self.labelTraining = labelTraining
        self.iterationsNum = iterationsNum

        #do shuffle
        zipMapLabelEx = list(zip(self.exTraining, self.labelTraining))
        random.shuffle(zipMapLabelEx)
        self.exampleAfterShuffle, self.labelAfterShuffle = zip(*zipMapLabelEx)

        # Create empty weight vectors
        self.weight_vectors = np.zeros((3, 8))

    def train(self):
        lamda = 0.01
        ETA = 0.1
        for i in range(self.iterationsNum):
            for xi, yi in zip(self.exampleAfterShuffle, self.labelAfterShuffle):
                classesF = np.zeros((3,), dtype=int)
                argMax = 0
                predicted_class = 0

                #find the arg max
                for classI in range(3):
                    y_Tag = np.dot(xi, self.weight_vectors[classI])
                    if (y_Tag >= argMax):
                        argMax = y_Tag
                        predicted_class = classI

                classesF[yi] = 1;
                classesF[predicted_class] = 1;
                # find the class that is not yi and not the predicted_class
                for c in range(3):
                    if classesF[c] == 0:
                        otherClass = c
                # coefficient = 1 − ηλ
                coefficient = 1 - ETA * lamda
                #update rule
                if not (yi == predicted_class):
                    self.weight_vectors[yi] = coefficient * self.weight_vectors[yi] + ETA * xi
                    self.weight_vectors[predicted_class] = coefficient * self.weight_vectors[predicted_class] - ETA * xi
                    self.weight_vectors[otherClass] = coefficient * self.weight_vectors[otherClass]
                #update if there is not mistake
                else:
                    for c in range(3):
                        self.weight_vectors[c, :] = coefficient * self.weight_vectors[c, :]

            ETA = ETA / (i + 1)
        return self.weight_vectors
