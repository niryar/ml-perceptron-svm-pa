#nir yarkoni 313587057
import numpy as np
import random

ITERATIONS = 50


class Perceptron(object):
    def __init__(self, exTraining, labelTraining, iterationsNum=ITERATIONS):
        self.exTraining = exTraining
        self.labelTraining = labelTraining
        self.iterationsNum = iterationsNum

        #do shuffle
        zipMapLabelEx = list(zip(self.exTraining, self.labelTraining))
        random.shuffle(zipMapLabelEx)
        self.exampleAfterShuffle, self.labelAfterShuffle = zip(*zipMapLabelEx)

        # Create empty weight vectors
        self.weight_vectors = np.zeros((3, 8))

    def train(self):
        ETA = 0.1
        for i in range(self.iterationsNum):
            for xi, yi in zip(self.exampleAfterShuffle, self.labelAfterShuffle):
                argMax = 0
                predicted_class = 0

                #find the arg max
                for classI in range(3):
                    y_Tag = np.dot(xi, self.weight_vectors[classI])
                    if (y_Tag >= argMax):
                        argMax = y_Tag
                        predicted_class = classI
                #update rule
                if not (yi == predicted_class):
                    self.weight_vectors[yi] += ETA * xi  # the right class
                    self.weight_vectors[predicted_class] -= ETA * xi  # the predicted class

            ETA = ETA / (i + 1)
        return self.weight_vectors
