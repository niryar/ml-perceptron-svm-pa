#nir yarkoni 313587057
import numpy as np
import random

ITERATIONS = 20


class PassiveAggressive(object):
    def __init__(self, exTraining, labelTraining, iterationsNum=ITERATIONS):
        self.exTraining = exTraining
        self.labelTraining = labelTraining
        self.iterationsNum = iterationsNum

        #do shuffle
        zipMapLabelEx = list(zip(self.exTraining, self.labelTraining))
        random.shuffle(zipMapLabelEx)
        self.exampleAfterShuffle, self.labelAfterShuffle = zip(*zipMapLabelEx)

        # Create empty weight vectors
        self.weight_vectors = np.zeros((3, 8))

    def train(self):
        Counter = 0
        weight_sum = self.weight_vectors
        for i in range(self.iterationsNum):
            for xi, yi in zip(self.exampleAfterShuffle, self.labelAfterShuffle):
                argMax = 0
                predicted_class = 0
                #find the arg max
                for classI in range(3):
                    y_Tag = np.dot(xi, self.weight_vectors[classI])
                    if (y_Tag >= argMax):
                        argMax = y_Tag
                        predicted_class = classI

                if not (yi == predicted_class):
                    #calculate the loss func
                    loss = max(0, 1 - np.dot(self.weight_vectors[yi], xi) + np.dot(self.weight_vectors[predicted_class],
                                                                                   xi))
                    x = np.linalg.norm(xi)
                    teta = 0
                    if (x != 0):
                        teta = loss / (2 * np.power(x, 2))
                    #update rule
                    self.weight_vectors[yi] += np.dot(teta, xi)
                    self.weight_vectors[predicted_class] -= np.dot(teta, xi)
                    # do cross validation
                    weight_sum = weight_sum + self.weight_vectors
                    Counter += 1
        # the average of w
        if (Counter != 0):
            self.weight_vectors = weight_sum / Counter
        return self.weight_vectors
